﻿// Adapted by Paul Carver, RPi.DotNetKickstart

// Copyright (c) 2014 Adafruit Industries
// Author: Tony DiCola
// Based on code from Gert van Loo & Dom: http://elinux.org/RPi_Low-level_peripherals#GPIO_Code_examples

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef RPICOMMON_H
#define RPICOMMON_H

#include <stdint.h>

#define MMIO_SUCCESS 0
#define MMIO_ERROR_DEVMEM -1
#define MMIO_ERROR_MMAP -2
#define MMIO_ERROR_OFFSET -3

volatile uint32_t* pi_2_mmio_gpio;

extern int pi_2_mmio_init(void);

extern inline void pi_2_mmio_set_input(const int gpio_number) {
  // Set GPIO register to 000 for specified GPIO number.
  *(pi_2_mmio_gpio+((gpio_number)/10)) &= ~(7<<(((gpio_number)%10)*3));
}

extern inline void pi_2_mmio_set_output(const int gpio_number) {
  // First set to 000 using input function.
  pi_2_mmio_set_input(gpio_number);
  // Next set bit 0 to 1 to set output.
  *(pi_2_mmio_gpio+((gpio_number)/10)) |=  (1<<(((gpio_number)%10)*3));
}

extern inline void pi_2_mmio_set_high(const int gpio_number) {
  *(pi_2_mmio_gpio+7) = 1 << gpio_number;
}

extern inline void pi_2_mmio_set_low(const int gpio_number) {
  *(pi_2_mmio_gpio+10) = 1 << gpio_number;
}

extern inline uint32_t pi_2_mmio_input(const int gpio_number) {
  return *(pi_2_mmio_gpio+13) & (1 << gpio_number);
}

// Busy wait delay for most accurate timing, but high CPU usage.
// Only use this for short periods of time (a few hundred milliseconds at most)!
extern void busy_wait_milliseconds(uint32_t millis);

// General delay that sleeps so CPU usage is low, but accuracy is potentially bad.
extern void sleep_milliseconds(uint32_t millis);

// Increase scheduling priority and algorithm to try to get 'real time' results.
extern void set_max_priority(void);

// Drop scheduling priority back to normal/default.
extern void set_default_priority(void);

#endif
