﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Text;
using System.Runtime.InteropServices;

namespace PaulTechGuy.RPi.WiringPi
{
	public static class SPI
	{
		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSPIGetFd")]
		public static extern int WiringPiSPIGetFd(int channel);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		/// <example>
		/// StringBuilder data = new StringBuilder(100);
		/// WiringPiSPIDataRW(channel, data, data.Capacity);
		/// </example>
		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSPIDataRW")]
		public static extern int WiringPiSPIDataRW(int channel, StringBuilder data);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSPISetupMode")]
		public static extern int WiringPiSPISetupMode(int channel, int speed, int mode);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSPISetup")]
		public static extern int WiringPiSPISetup(int channel, int speed);

	}
}