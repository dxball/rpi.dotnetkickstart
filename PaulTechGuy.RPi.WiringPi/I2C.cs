﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Runtime.InteropServices;
using System.Text;

namespace PaulTechGuy.RPi.WiringPi
{
	public static class I2C
	{
		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CSetup")]
		public static extern int WiringPiI2CSetup(int devId);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CSetupInterface")]
		public static extern int WiringPiI2CSetupInterface(StringBuilder device, int devId);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CRead")]
		public static extern int WiringPiI2CRead(int fd);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CReadReg8")]
		public static extern int WiringPiI2CReadReg8(int fd, int reg);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CReadReg16")]
		public static extern int WiringPiI2CReadReg16(int fd, int reg);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CWrite")]
		public static extern int WiringPiI2CWrite(int fd, int data);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CWriteReg8")]
		public static extern int WiringPiI2CWriteReg8(int fd, int reg, int data);


		[DllImport("libwiringPi.so", EntryPoint = "wiringPiI2CWriteReg16")]
		public static extern int WiringPiI2CWriteReg16(int fd, int reg, int data);
	}
}
