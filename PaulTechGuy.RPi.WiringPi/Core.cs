﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Runtime.InteropServices;

namespace PaulTechGuy.RPi.WiringPi
{
	public static class Core
	{
		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSetup")]
		public static extern int WiringPiSetup();

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSetupGpio")]
		public static extern int WiringPiSetupGpio();

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSetupSys")]
		public static extern int WiringPiSetupSys();

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiSetupPhys")]
		public static extern int WiringPiSetupPhys();

		[DllImport("libwiringPi.so", EntryPoint = "pinModeAlt")]
		public static extern void PinModeAlt(int pin, int mode);

		[DllImport("libwiringPi.so", EntryPoint = "pinMode")]
		public static extern void PinMode(int pin, int mode);

		[DllImport("libwiringPi.so", EntryPoint = "pullUpDnControl")]
		public static extern void PullUpDnControl(int pin, int pud);


		[DllImport("libwiringPi.so", EntryPoint = "digitalRead")]
		public static extern int DigitalRead(int pin);

		[DllImport("libwiringPi.so", EntryPoint = "digitalWrite")]
		public static extern void DigitalWrite(int pin, int value);

		[DllImport("libwiringPi.so", EntryPoint = "pwmWrite")]
		public static extern void PwmWrite(int pin, int value);

		[DllImport("libwiringPi.so", EntryPoint = "analogRead")]
		public static extern int AnalogRead(int pin);

		[DllImport("libwiringPi.so", EntryPoint = "analogWrite")]
		public static extern void AnalogWrite(int pin, int value);

		[DllImport("libwiringPi.so", EntryPoint = "piBoardRev")]
		public static extern int PiBoardRev();

		[DllImport("libwiringPi.so", EntryPoint = "piBoardId")]
		public static extern void PiBoardId(ref int model, ref int version, ref int mem, ref int maker, ref int overVolted);

		[DllImport("libwiringPi.so", EntryPoint = "wpiPinToGpio")]
		public static extern int wpiPinToGpio(int wpiPin);

		[DllImport("libwiringPi.so", EntryPoint = "physPinToGpio")]
		public static extern int PhysPinToGpio(int physPin);

		[DllImport("libwiringPi.so", EntryPoint = "setPadDrive")]
		public static extern void SetPadDrive(int group, int value);

		[DllImport("libwiringPi.so", EntryPoint = "getAlt")]
		public static extern int GetAlt(int pin);

		[DllImport("libwiringPi.so", EntryPoint = "pwmToneWrite")]
		public static extern void PwmToneWrite(int pin, int freq);

		[DllImport("libwiringPi.so", EntryPoint = "digitalWriteByte")]
		public static extern void DigitalWriteByte(int value);

		[DllImport("libwiringPi.so", EntryPoint = "pwmSetMode")]
		public static extern void PwmSetMode(int mode);

		[DllImport("libwiringPi.so", EntryPoint = "pwmSetRange")]
		public static extern void PwmSetRange(uint range);

		[DllImport("libwiringPi.so", EntryPoint = "pwmSetClock")]
		public static extern void PwmSetClock(int divisor);

		[DllImport("libwiringPi.so", EntryPoint = "gpioClockSet")]
		public static extern void GpioClockSet(int pin, int freq);

		[Obsolete("Use the new wiringPiISR method", error: true)]
		[DllImport("libwiringPi.so", EntryPoint = "waitForInterrupt")]
		public static extern int WaitForInterrupt(int pin, int timeoutMs);

		[DllImport("libwiringPi.so", EntryPoint = "wiringPiISR")]
		public static extern int WiringPiISR(int pin, int mode, Action action);

		[DllImport("libwiringPi.so", EntryPoint = "piHiPri")]
		public static extern int PiHiPri(int priority);

	}
}