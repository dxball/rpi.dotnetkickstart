﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Runtime.InteropServices;

namespace PaulTechGuy.RPi.DHT11TempSensor
{
	class Program
	{
		[DllImport("libptgrpi.so", EntryPoint = "dht_read")]
		public static extern int dht_read(int type, int pin, ref float humidity, ref float temperature);

		static void Main(string[] args)
		{
			new Program().Run(args);
		}

		private void Run(string[] args)
		{
			float humidity = 0;
			float temperature = 0;
			int pin = 17;

			// Or use DH22 (22) for the DTH22 sensor
			int DH11 = 11;

			// we call the C dht read since Mono/C# is just not accurate/fast enough to keep up with
			// the DHTxx pulsing
			int status = dht_read(DH11, pin, ref humidity, ref temperature);
			if (status == 0)
			{
				// success
				Console.WriteLine("Temperature: {0}C, Humidity: {1}%", temperature, humidity);
			}
			else
			{
				Console.WriteLine("Error reading DHxx sensor: {0}", status);
			}
		}

	}
}
