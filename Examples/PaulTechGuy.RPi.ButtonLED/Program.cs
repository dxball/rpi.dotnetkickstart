﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Threading;
using System.Threading.Tasks;
using PaulTechGuy.RPi.GpioLib;

namespace PaulTechGuy.RPi.ButtonLED
{
	class Program
	{
		private volatile bool LedEnabled = true;

		static void Main(string[] args)
		{
			new Program().Run(args);
		}

		private void clickHandler()
		{
			Console.WriteLine("click handler called");
		}
		private void Run(string[] args)
		{
			// Note: GpioHeader defaults to PinType.Broadcom; use SetPinType to change

			Task[] tasks = new Task[2];

			CancellationTokenSource ledCts = new CancellationTokenSource();
			tasks[0] = Task.Factory.StartNew(() => RunLedTask(ledCts.Token), ledCts.Token);

			CancellationTokenSource buttonCts = new CancellationTokenSource();
			tasks[1] = Task.Factory.StartNew(() => RunButtonTask(buttonCts.Token), buttonCts.Token);

			Console.WriteLine("Press Enter to quit...");
			Console.ReadLine();

			ledCts.Cancel();
			buttonCts.Cancel();
			Task.WaitAll(tasks);
		}

		private void RunButtonTask(CancellationToken cancelToken)
		{
			int buttonNumber = 2;
			using (DigitalPin pin = GpioHeader.Instance.CreatePin(buttonNumber, DigitalPinDirection.Input, PinValue.Low, PullUpDownType.Down))
			{
				pin.Interrupt(EdgeType.Falling, cancelToken, p =>
				{
					Console.WriteLine("Button pressed");
					LedEnabled = !LedEnabled;
				});
				WaitHandle.WaitAll(new WaitHandle[] { cancelToken.WaitHandle });
			}
		}

		private void RunLedTask(CancellationToken cancelToken)
		{
			// note that we start with the led off (no blinking)

			int ledNumber = 18;
			using (DigitalPin pin = GpioHeader.Instance.CreatePin(ledNumber, DigitalPinDirection.Output))
			{
				while (!cancelToken.IsCancellationRequested)
				{
					if (LedEnabled)
					{
						int blinkTimeMs = 250;
						bool ledValue = true;

						// keep track of the led state when we started...if it is different then we are
						// toggling from blink to non-blink or visa versa
						bool oldLedState = LedEnabled;

						while (true)
						{
							pin.Output(ledValue ? PinValue.High : PinValue.Low);
							ledValue = !ledValue;

							WaitHandle.WaitAll(new WaitHandle[] { cancelToken.WaitHandle }, blinkTimeMs);
							if (cancelToken.IsCancellationRequested)
							{
								break;
							}
							else if (oldLedState != LedEnabled)
							{
								// if the old state was on, turn it off
								if (oldLedState)
								{
									pin.Output(PinValue.Low); // turn it off before pausing
								}
								break;
							}
						}

					}
					else
					{
						// this is max amount of time before we'll check to see
						// our state has changed
						int sleepTimeMs = 1000;

						WaitHandle.WaitAny(new WaitHandle[] { cancelToken.WaitHandle }, sleepTimeMs);
					}
				}

				// all done...make sure we're off
				pin.Output(PinValue.Low);
			}
		}

	}
}
