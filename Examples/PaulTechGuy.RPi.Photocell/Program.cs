﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Threading;
using System.Threading.Tasks;
using PaulTechGuy.RPi.GpioLib;

namespace PaulTechGuy.RPi.Photocell
{
	class Program
	{
		private volatile int _readCount = 0;
		private volatile int _maxReads = Int32.MinValue;
		private ManualResetEvent _ledWakeup = new ManualResetEvent(false);

		static void Main(string[] args)
		{
			new Program().Run(args);
		}

		private void Run(string[] args)
		{
			Task[] tasks = new Task[2];

			CancellationTokenSource ledCts = new CancellationTokenSource();
			tasks[0] = Task.Factory.StartNew(() => RunLedTask(ledCts.Token), ledCts.Token);

			CancellationTokenSource photoCts = new CancellationTokenSource();
			tasks[1] = Task.Factory.StartNew(() => RunPhotocellTask(photoCts.Token), photoCts.Token);

			Console.WriteLine("Move your hand over the photocell back and forth.\n\nPress Enter to quit...");
			Console.ReadLine();

			ledCts.Cancel();
			photoCts.Cancel();
			Task.WaitAll(tasks);
		}

		private void RunLedTask(CancellationToken cancelToken)
		{
			int ledPin1 = 17;
			int ledPin2 = 27;
			using (DigitalPin pin1 = GpioHeader.Instance.CreatePin(ledPin1, DigitalPinDirection.Output, PinValue.Low))
			using (DigitalPin pin2 = GpioHeader.Instance.CreatePin(ledPin2, DigitalPinDirection.Output, PinValue.Low))
			{
				while (true)
				{
					_ledWakeup.Reset();
					if (_ledWakeup.WaitOne(200))
					{
						if (_readCount < (_maxReads / 2))
						{
							pin1.Output(PinValue.High);
							pin2.Output(PinValue.Low);
						}
						else
						{
							pin1.Output(PinValue.Low);
							pin2.Output(PinValue.High);
						}
					}
					else if (cancelToken.IsCancellationRequested)
					{
						pin1.Output(PinValue.Low);
						pin2.Output(PinValue.Low);
						break;
					}
				}
			}
		}

		private void RunPhotocellTask(CancellationToken cancelToken)
		{
			Random rnd = new Random();
			int photoPin = 18;
			using (AnalogPin pin = GpioHeader.Instance.CreatePin(photoPin, AnalogPinDirection.Output, 0))
			{
				while (!cancelToken.IsCancellationRequested)
				{
					System.Threading.Thread.Sleep(200);
					pin.ChangeDirection(AnalogPinDirection.Input);

					int reads = 0;
					while (!cancelToken.IsCancellationRequested && pin.Input() == 0)
					{
						++reads;
					}

					_readCount = reads;

					// every so often allow max reads to be reset (fixes times when max
					// read goes very high in the dark)
					if (rnd.Next(0, 20) == 0) _maxReads = reads;
					_maxReads = Math.Max(_maxReads, reads);

					pin.ChangeDirection(AnalogPinDirection.Output);
					pin.Output(0);

					Console.WriteLine("Reads: {0}, Max: {1}", _readCount, _maxReads);
					_ledWakeup.Set();
				}
			}
		}
	}
}
