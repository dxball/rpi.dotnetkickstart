﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using PaulTechGuy.RPi.GpioLib;

namespace PaulTechGuy.RPi.PWMLED
{
	class Program
	{
		static void Main(string[] args)
		{
			new Program().Run(args);
		}

		private void Run(string[] args)
		{
			Console.WriteLine("Press any key to exit...");

			int ledNumber = 17;
			using (AnalogPin pin = GpioHeader.Instance.CreatePin(ledNumber, AnalogPinDirection.SoftPwmOutput))
			{
				int loops = 100;
				while (!Console.KeyAvailable)
				{
					for (int i = 0; i < loops; ++i)
					{
						pin.Output(i);
						System.Threading.Thread.Sleep(10);
						if (Console.KeyAvailable) break;
					}

					for (int i = loops - 1; i >= 0; --i)
					{
						pin.Output(i);
						System.Threading.Thread.Sleep(10);
						if (Console.KeyAvailable) break;
					}
				}
			}
		}
	}
}
