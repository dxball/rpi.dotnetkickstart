﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Linq;
using PaulTechGuy.RPi.GpioLib;

namespace PaulTechGuy.RPi.PWMRGBLED
{
	class Program
	{
		static void Main(string[] args)
		{
			new Program().Run(args);
		}

		private void Run(string[] args)
		{
			Console.WriteLine("Press any key to exit...");

			int[] ledNumbers = new int[] { 17, 18, 27 };

			using (AnalogPin pin1 = GpioHeader.Instance.CreatePin(ledNumbers[0], AnalogPinDirection.SoftPwmOutput))
			using (AnalogPin pin2 = GpioHeader.Instance.CreatePin(ledNumbers[1], AnalogPinDirection.SoftPwmOutput))
			using (AnalogPin pin3 = GpioHeader.Instance.CreatePin(ledNumbers[2], AnalogPinDirection.SoftPwmOutput))
			{
				Random rnd = new Random();
				AnalogPin[] pins = new AnalogPin[] { pin1, pin2, pin3 };

				while (!Console.KeyAvailable)
				{
					AnalogPin[] randomPins = pins
						.OrderBy(p => rnd.Next(99))
						.Take(rnd.Next(1, 4))
						.ToArray();
					int sleepMs = rnd.Next(10, 30);
					int rate = rnd.Next(1, 5);

					TurnAllOff(pins);
					for (int i = 1; !Console.KeyAvailable && i < 100; i += rate)
					{
						SetRGBColor(randomPins, i);
						System.Threading.Thread.Sleep(sleepMs);
					}

					for (int i = 100; !Console.KeyAvailable && i > 0; i -= rate)
					{
						SetRGBColor(randomPins, i);
						System.Threading.Thread.Sleep(sleepMs);
					}
				}

				TurnAllOff(pins);
			}
		}

		private static void TurnAllOff(AnalogPin[] pins)
		{
			// all done...turn everything off
			for (int i = 0; i < pins.Length; ++i)
			{
				pins[i].Output(0);
			}
		}

		private void SetRGBColor(AnalogPin[] pins, int value)
		{
			for (int i = 0; i < pins.Length; ++i)
			{
				pins[i].Output(value);
			}
		}
	}
}
