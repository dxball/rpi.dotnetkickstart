﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using PaulTechGuy.RPi.GpioLib;

namespace PaulTechGuy.RPi.TMP102TempSensor
{
	class Program
	{
		static void Main(string[] args)
		{
			new Program().Run(args);
		}

		private void Run(string[] args)
		{
			//
			// see the end of this file fore information on reading the TMP102
			//

			int id = 0x48;
			using (I2C i2c = new I2C(id))
			{
				do
				{
					// TMP102 requires two bytes to be read (16 bits)
					short data = (short)i2c.Read16(0);

					// we have the two bytes of temp data in an int; conver it to a short
					short s = (short)data;

					// the MSB is actually the second byte so we need to zero out the first byte
					byte msb = (byte)(s & 0x00FF);

					// the lsb is first; move it to the right (which also zeros out the left)
					byte lsb = (byte)(s >> 8);

					// now combine then back together with MSB first
					int temperature = ((msb << 8) | lsb) >> 4;

					Console.WriteLine(
						"\n\nTemperature is {0}°C, {1}°F",
						temperature * .0625,
						(temperature * .0625 * 1.8) + 32);

					Console.Write("Press any key to read temperature, or Enter to quit...");
				} while (Console.ReadKey(true).Key != ConsoleKey.Enter);
			}
			Console.WriteLine();
		}
	}
}

// This data comes from the TMP102 documentation.
//
// The Temperature Register of the TMP102 is configured as a 12-bit, read-only register
// (Configuration Register EM bit = '0', see the Extended Mode section), or as a 13-bit,
// read-only register (Configuration Register EM bit = '1') that stores the output of the
// most recent conversion.  Two bytes must be read to obtain data, and are described in
// Table 3 and Table 4. Note that byte 1 is the most significant byte, followed by
// byte 2, the least significant byte.  The first 12 bits (13 bits in Extended mode) are
// used to indicate temperature.  The least significant byte does not have to be read if
// that information is not needed.  The data format for temperature is summarized in
// Table 5 and Table 6. One  LSB equals 0.0625°C.  Negative numbers are represented in
// binary twos complement format.
