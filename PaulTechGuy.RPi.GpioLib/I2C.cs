﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Text;
using wpi = PaulTechGuy.RPi.WiringPi;

namespace PaulTechGuy.RPi.GpioLib
{
	public class I2C : IDisposable
	{
		private bool _isDisposed = false;

		private readonly int _fd;
		private readonly int _deviceId;
		private readonly string _deviceName;

		public I2C(int deviceId)
		{
			_fd = wpi.I2C.WiringPiI2CSetup(deviceId);
			if (_fd == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error setting up I2C");
			}
			_deviceId = deviceId;
		}

		public I2C(string deviceName, int deviceId)
		{
			_fd = wpi.I2C.WiringPiI2CSetupInterface(new StringBuilder(deviceName), deviceId);
			if (_fd == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error setting up I2C interface");
			}
			_deviceId = deviceId;
			_deviceName = deviceName;
		}

		public int Read()
		{
			int data = wpi.I2C.WiringPiI2CRead(_fd);
			if (data == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error performing Read");
			}

			return data;
		}

		public int Read8(int register)
		{
			int data = wpi.I2C.WiringPiI2CReadReg8(_fd, register);
			if (data == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error performing Read8 register");
			}

			return data;
		}

		public int Read16(int register)
		{
			int data = wpi.I2C.WiringPiI2CReadReg16(_fd, register);
			if (data == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error performing Read16 register");
			}

			return data;
		}

		public void Write(int data)
		{
			int status = wpi.I2C.WiringPiI2CWrite(_fd, data);
			if (data == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error performing Write");
			}
		}

		public void Write8(int register, int data)
		{
			int status = wpi.I2C.WiringPiI2CWriteReg8(_fd, register, data);
			if (data == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error performing Write8");
			}
		}

		public void Write16(int register, int data)
		{
			int status = wpi.I2C.WiringPiI2CWriteReg16(_fd, register, data);
			if (data == wpi.Constant.ErrorReturn)
			{
				throw new GpioLibException("Error performing Write16");
			}
		}

		#region IDisposable

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				// free other managed resources
				Cleanup();
			}

			_isDisposed = true;
		}

		private void Cleanup()
		{
		}

		#endregion

	}
}
