﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using wpi = PaulTechGuy.RPi.WiringPi;

namespace PaulTechGuy.RPi.GpioLib
{
	public class BasePin : IDisposable
	{
		public int Number { get; private set; }

		public int Direction { get; private set; }

		private bool _isDisposed = false;

		internal BasePin(int number, int direction)
		{
			Number = number;
			Direction = direction;

			wpi.Core.PinMode(Number, direction);
		}

		internal BasePin(int number, int direction, int value)
			: this(number, direction)
		{
			wpi.Core.DigitalWrite(Number, value);
		}

		internal BasePin(int number, int direction, int value, int pullUpDown)
			: this(number, direction)
		{
			wpi.Core.PullUpDnControl(Number, pullUpDown);
		}

		internal void Output(int value)
		{
			wpi.Core.DigitalWrite(Number, value);
		}

		internal int Input()
		{
			return wpi.Core.DigitalRead(Number);
		}

		internal void ChangeDirection(int direction)
		{
			Direction = direction;

			wpi.Core.PinMode(Number, direction);
		}

		public void Interrupt(EdgeType mode, CancellationToken cancelToken, Action<BasePin> action)
		{
			string cmdLine = string.Format("edge {0} {1}", Number, mode.ToString().ToLower());
			ProcessStartInfo startInfo = new ProcessStartInfo("gpio", cmdLine);
			startInfo.CreateNoWindow = true;
			startInfo.UseShellExecute = false;
			using (Process p = Process.Start(startInfo))
			{
				p.WaitForExit();
				if (p.ExitCode != 0)
				{
					throw new GpioLibException("Bad exit code from wiringPi:gpio utility: " + p.ExitCode.ToString());
				}
			}

			FileSystemWatcher fsw = new FileSystemWatcher(SystemPath, "value");
			fsw.NotifyFilter = NotifyFilters.LastWrite;
			fsw.EnableRaisingEvents = true;

			while (!cancelToken.IsCancellationRequested)
			{
				WaitForChangedResult result = fsw.WaitForChanged(WatcherChangeTypes.Changed, 250);
				if (!result.TimedOut)
				{
					action(this);
				}
			}
		}
		internal string SystemPath
		{
			get
			{
				string path = Path.Combine(GpioHeader.GpioPath, string.Format("gpio{0}", Number));

				return path;
			}
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				// free other managed resources
				Cleanup();
			}

			_isDisposed = true;
		}

		private void Cleanup()
		{
		}

	}
}
