﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using wpi = PaulTechGuy.RPi.WiringPi;

namespace PaulTechGuy.RPi.GpioLib
{
	public class AnalogPin : BasePin, IDisposable
	{
		public new AnalogPinDirection Direction { get; private set; }

		private bool _isDisposed = false;

		internal AnalogPin(int number, AnalogPinDirection direction)
			: base(number, (int)direction)
		{
			Direction = direction;

			// Note: Looks like wiringPi will call additonal methods like SoftPwmCreate and SoftToneCreate
			// for us depending on direction...so we don't do that here
		}

		internal AnalogPin(int number, AnalogPinDirection direction, int value)
			: this(number, direction)
		{
			this.Output(value);
		}

		public new void Output(int value)
		{
			switch (Direction)
			{
				case AnalogPinDirection.Output:
				case AnalogPinDirection.PwmOutput:
				case AnalogPinDirection.GpioClock:
				case AnalogPinDirection.PwmToneOutput:
					base.Output(value);
					break;

				case AnalogPinDirection.SoftPwmOutput:
					wpi.SoftPWM.SoftPwmWrite(base.Number, value);
					break;

				case AnalogPinDirection.SoftToneOutput:
					wpi.SoftTone.SoftToneWrite(base.Number, value);
					break;

				default:
					throw new GpioLibException("Unhandled Direction: " + Direction);
			}
		}

		public new int Input()
		{
			return base.Input();
		}

		public void ChangeDirection(AnalogPinDirection direction)
		{
			base.ChangeDirection((int)direction);
		}

		public new void Dispose()
		{
			Dispose(true);
		}

		protected new virtual void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				// free other managed resources
				Cleanup();
			}

			_isDisposed = true;
		}

		private void Cleanup()
		{
			switch (Direction)
			{
				case AnalogPinDirection.Input:
				case AnalogPinDirection.Output:
				case AnalogPinDirection.PwmOutput:
				case AnalogPinDirection.GpioClock:
				case AnalogPinDirection.PwmToneOutput:
					break;

				case AnalogPinDirection.SoftPwmOutput:
					wpi.SoftPWM.SoftPwmStop(Number);
					break;

				case AnalogPinDirection.SoftToneOutput:
					wpi.SoftTone.SoftToneStop(Number);
					break;

				default:
					throw new GpioLibException("Unhandled Direction: " + Direction);
			}
		}

	}
}
