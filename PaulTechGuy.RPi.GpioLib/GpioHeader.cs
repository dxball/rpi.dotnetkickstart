﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Runtime.InteropServices;
using System.Threading;
using wpi = PaulTechGuy.RPi.WiringPi;

namespace PaulTechGuy.RPi.GpioLib
{
	public class GpioHeader
	{
		internal readonly static string GpioPath = "/sys/class/gpio";
		private static readonly Lazy<GpioHeader> _instance = new Lazy<GpioHeader>(() => new GpioHeader(), LazyThreadSafetyMode.ExecutionAndPublication);

		public static GpioHeader Instance
		{
			get
			{
				return _instance.Value;
			}
		}

		private GpioHeader()
		{
			// assume broadcom as the default
			InitializeHeader(PinType.Broadcom);
		}

		public void SetPinType(PinType type)
		{
			InitializeHeader(type);
		}

		public DigitalPin CreatePin(int number, DigitalPinDirection direction)
		{
			return new DigitalPin(number, direction);
		}

		public DigitalPin CreatePin(int number, DigitalPinDirection direction, PinValue value)
		{
			return new DigitalPin(number, direction, value);
		}

		public DigitalPin CreatePin(int number, DigitalPinDirection direction, PinValue value, PullUpDownType pullUpDown)
		{
			return new DigitalPin(number, direction, value, pullUpDown);
		}

		public AnalogPin CreatePin(int number, AnalogPinDirection direction)
		{
			return new AnalogPin(number, direction);
		}

		public AnalogPin CreatePin(int number, AnalogPinDirection direction, int value)
		{
			return new AnalogPin(number, direction, value);
		}

		private void InitializeHeader(PinType type)
		{
			switch (type)
			{
				case PinType.WiringPi:
					wpi.Core.WiringPiSetup();
					break;

				case PinType.Broadcom:
					wpi.Core.WiringPiSetupGpio();
					break;

				case PinType.Physical:
					wpi.Core.WiringPiSetupPhys();
					break;

				case PinType.FileSystem:
					wpi.Core.WiringPiSetupSys();
					break;

				default:
					throw new GpioLibException("Invalid PinMode: " + type.ToString());
			}
		}

		public static BoardInformation BoardInformation()
		{
			int model = 0;
			int version = 0;
			int memory = 0;
			int maker = 0;
			int overVolted = 0;
			wpi.Core.PiBoardId(ref model, ref version, ref memory, ref maker, ref overVolted);

			int revision = wpi.Core.PiBoardRev();

			BoardInformation bi = new BoardInformation
			{
				Model = (BoardModel)model,
				Maker = (BoardMaker)maker,
				Version = (BoardVersion)version,
				Memory = memory,
				OverVolted = overVolted != 0 ? true : false,
			};

			return bi;
		}
	}
}
