﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;

namespace PaulTechGuy.RPi.GpioLib
{
	public class DigitalPin : BasePin, IDisposable
	{
		public new DigitalPinDirection Direction { get; private set; }

		private bool _isDisposed = false;

		internal DigitalPin(int number, DigitalPinDirection direction)
			: base(number, (int)direction)
		{
			Direction = direction;
		}

		internal DigitalPin(int number, DigitalPinDirection direction, PinValue value)
			: base(number, (int)direction, (int)value)
		{
			Direction = direction;
		}

		internal DigitalPin(int number, DigitalPinDirection direction, PinValue value, PullUpDownType pullUpDown)
			: base(number, (int)direction, (int)value, (int)pullUpDown)
		{
			Direction = direction;
		}

		public void Output(PinValue value)
		{
			base.Output((int)value);
		}

		public new PinValue Input()
		{
			return (PinValue)base.Input();
		}

		public void ChangeDirection(DigitalPinDirection direction)
		{
			base.ChangeDirection((int)direction);
		}

		public new void Dispose()
		{
			Dispose(true);
		}

		protected new virtual void Dispose(bool disposing)
		{
			if (_isDisposed)
			{
				return;
			}

			if (disposing)
			{
				// free other managed resources
				Cleanup();
			}

			_isDisposed = true;
		}

		private void Cleanup()
		{
		}

	}
}
