﻿// PaulTechGuy.RPi.DotNetKickstart
//
// C# / Mono programming library for the Raspberry Pi
// Copyright (c) 2015 Paul Carver
//
// http://paultechguy/rpi/dotnetkickstart
//
// DotNetKickstart is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Runtime.InteropServices;
using wpi = PaulTechGuy.RPi.WiringPi;

namespace PaulTechGuy.RPi.GpioLib
{
	public static class OS
	{
		public enum PriorityWhich
		{
			Process = 0,
			ProcessGroup = 1,
			User = 2,
		}

		[DllImport("libc.so.6", EntryPoint = "usleep")]
		private static extern int usleep(uint amount);

		[DllImport("libc.so.6", EntryPoint = "setpriority")]
		private static extern int setpriority(int which, int who, int prio);

		public static int SleepMilliseconds(uint msec)
		{
			int status = usleep(msec * 1000);
			if (status != 0)
			{
				throw new GpioLibException("SleepMilliseconds returned error: " + status);
			}

			return status;
		}

		public static int SleepMicroseconds(uint usec)
		{
			int status = usleep(usec);
			if (status != 0)
			{
				throw new GpioLibException("SleepMicroseconds returned error: " + status);
			}

			return status;
		}

		public static int SetPriority(PriorityWhich which, int who, int priority)
		{
			int status = setpriority((int)which, who, priority);
			if (status != 0)
			{
				throw new GpioLibException("SetPriority returned error: " + status);
			}

			return status;
		}

	}
}
